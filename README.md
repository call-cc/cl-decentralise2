# cl-decentralise2

cl-decentralise2 is a library for writing distributed object storing programs.
This library provides a way to create services that synchronise data very
quickly (both in terms of programmer speed and program speed), including basic
support for event streaming using channels and node discovery.

cl-decentralise2 clients typically will create either a "system" which
interfaces inbuilt connection and acceptor classes to a program that handles
data validation and storage, a connection class that reads and writes
messages over some medium (such as TCP, a serial cable, carrier pigeon), or
a "client" that provides a synchronous, unmultiplexed abstraction over a 
connection (which follows a asynchronous, somewhat multiplexed protocol).

A text format for sockets that loosely follows the wire format of the
discontinued Nettle project has been implemented, as well as a client
and a system mixin that implement the object distribution and searching
algorithms of the [Kademlia](http://www.scs.stanford.edu/~dm/home/papers/kpos.pdf)
distributed hash-table.

## Getting started

The standard system can be subclassed with the `memory-database-mixin` to
quickly set up a complete system class for testing.

``` common-lisp
(ql:quickload :decentralise2)
(defclass my-system (decentralise-standard-system:standard-system
                     decentralise-system:memory-database-mixin)
  ())

(defvar *system* (make-instance 'my-system
                  :acceptors (list
                              (make-instance 'decentralise-acceptors:socket-acceptor
                               :connection-class 'decentralise-connections:netfarm-format-connection
                               :key "/tmp/key.pem"
                               :certificate "/tmp/cert.pem"))))

(decentralise-system:start-system *system*)
```


``` common-lisp
cl-user> (decentralise-client:connect-to-uri "netfarm:localhost")
#<DECENTRALISE-CLIENT:CLIENT {12345678}>
cl-user> (decentralise-client:get-block * "foobar")
(1 ("channel-one" "channel-two") "Hello
World!")
cl-user> (decentralise-client:put-block ** "foobar2" 1 '("channel-three") "Goodbye world!")
t
cl-user> (decentralise-client:get-block *** "foobar2")
(1 ("channel-three") "Goodbye world!")
cl-user> (decentralise-system:stop-system *system*)
t
```
