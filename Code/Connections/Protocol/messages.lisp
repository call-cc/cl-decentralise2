(in-package :decentralise-connection)

;;; Writing messages

(defgeneric write-message (connection message)
  (:documentation "Write a message to a connection."))

(defun write-block (connection name version channels data)
  "Write a block (name, version, channels and data) to a connection."
  (write-message connection
                 (decentralise-messages:message :block
                                                name version channels data)))
(defun request-blocks (connection names)
  "Request the blocks named by strings in the list NAMES from a connection."
  (write-message connection
                 (decentralise-messages:message :get names)))
(defun request-block (connection name)
  "Request a single block named NAME from a connection."
  (request-blocks connection (list name)))

(defun write-ok (connection name)
  (write-message connection (decentralise-messages:message :ok name)))
(defun write-error (connection name reason)
  (write-message connection (decentralise-messages:message :error name reason)))
(defun allow-announcement (connection new-value)
  "Tell the connection it may announce your presence to other nodes if NEW-VALUE is true, or that it may not if it is false."
  (write-message connection (decentralise-messages:message :allow-announcement
                                                           new-value)))
(defun announce-node (connection uri id)
  "Announce the presence of a node to the connection."
  (write-message connection (decentralise-messages:message :announce uri id)))
(defun subscribe (connection subscriptions)
  "Send a connection a new list of subscriptions."
  (write-message connection (decentralise-messages:message :subscribe
                                                           subscriptions)))
(defun write-subscription (connection name version channels)
  "Inform a connection of a block they subscribed to."
  (write-message connection (decentralise-messages:message :subscription
                                                           name version channels)))

;;; Reading messages

(defgeneric read-message (connection)
  (:documentation "Read a message from a connection."))
(defgeneric handle-message (connection message)
  (:documentation "Handle an incoming message. The default method on CONNECTION is
to funcall the CONNECTION-MESSAGE-HANDLER of the connection with the message.")
  (:method ((connection connection) message)
    (funcall (connection-message-handler connection) message)))

(defmacro with-errors-blamed-on-connection ((connection &optional (log '*debug-io*))
                                            body &body extra-cleanup)
  "\"Blame\" errors on a connection by stopping it if anything bad happens. Also logs the condition to *debug-io* or the optional LOG value, then runs EXTRA-CLEANUP. If the connection isn't alive when we check, EXTRA-CLEANUP runs (though the connection is already stopped, so we don't stop it again)."
  (alexandria:once-only (connection log)
    `(if (not (connection-stopped-p ,connection))
         #-decentralise/dont-actually-blame
         (handler-case ,body
           (error (e)
             (format ,log "~&Error on connection ~a:~%~a~%" ,connection e)
             (stop-connection ,connection)
             . ,extra-cleanup))
         #+decentralise/dont-actually-blame
         ,body
         (progn . ,extra-cleanup))))
