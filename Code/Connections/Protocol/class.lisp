(in-package :decentralise-connection)

(defclass connection ()
  ((announcing :initform nil :accessor connection-announcing-p)
   (channels   :initform '() :accessor connection-channels)
   (created-by-me :initarg :created-by-me :initform nil
                  :reader connection-created-by-me-p)
   (destructors :initform '() :accessor connection-destructors)
   (id :initform nil :accessor connection-id)
   (message-handler :initarg :message-handler
                    :initform nil
                    :accessor connection-message-handler)
   (known-blocks :initform (make-hash-table :test 'equal)
                 :reader connection-known-blocks)
   (stopped :initform nil :accessor connection-stopped-p)
   (counters :initform (make-hash-table)
	     :reader connection-counters)))

(defmethod initialize-instance :after ((connection connection) &key)
  (labels ((myself (message)
             "Try to avoid small timing issues with message handlers."
             (loop while (eql (connection-message-handler connection) #'myself)
                   do (sleep 0.1))
             (connection-message-handler message)))
    (when (null (connection-message-handler connection))
      (setf (slot-value connection 'message-handler) #'myself))))

(defclass character-connection (connection) ()
  (:documentation "A connection that can send blocks with vectors of characters (strings) for data."))
(defmethod decentralise-messages:object->data ((data string)
                                               (connection character-connection)
                                               source)
  (declare (ignore source))
  data)
(defclass binary-connection (connection) ()
  (:documentation "A connection that can send blocks with vectors of octets for data."))
(defmethod decentralise-messages:object->data ((data vector)
                                               (connection binary-connection)
                                               source)
  (declare (ignore source))
  (if (equal (array-element-type data) '(unsigned-byte 8))
      data
      (call-next-method)))

;;; Counters

(defgeneric connection-counter-value (connection name)
  (:method ((connection connection) name)
    (values (gethash name (connection-counters connection) 0))))
(defgeneric (setf connection-counter-value) (new-value connection name)
  (:method (new-value (connection connection) name)
    (check-type new-value unsigned-byte)
    (setf (gethash name (connection-counters connection)) new-value)))
(defmacro with-connection-counter ((value connection name) &body body)
  (check-type connection symbol)
  `(let ((,value (connection-counter-value ,connection ',name)))
     (declare ((integer 0 *) ,value))
     (multiple-value-prog1
	 (progn ,@body)
       (incf (connection-counter-value ,connection ',name)))))

(define-method-combination hooks ()
  ((around (:around))
   (primary () :required t))
  (let ((primary-call-form
          `(progn ,@(loop for method in primary
                          collect `(call-method ,method)))))
    (if (null around)
        primary-call-form
        `(call-method ,(first around)
                      (,@(rest around) (make-method ,primary-call-form))))))

(defgeneric stop-connection (connection)
  (:method-combination hooks)
  (:method :around ((connection connection))
    (unless (connection-stopped-p connection)
      (setf (slot-value connection 'stopped) t)
      (call-next-method)))
  (:method ((connection connection))
    "Run all the destructors on a connection."
    (dolist (destructor (connection-destructors connection))
      (funcall destructor connection))))

;;; Known blocks

(defgeneric add-known-block (connection name version channels)
  (:method ((connection connection) name version channels)
    (setf (gethash name (connection-known-blocks connection))
          (cons version channels))))
(defgeneric map-known-blocks (function connection)
  (:method (function (connection connection))
    (maphash (lambda (name info)
               (destructuring-bind (version channels) info
                 (funcall function name version channels)))
             (connection-known-blocks connection))))
