(defpackage :decentralise-connection
  (:use :cl :decentralise-utilities :split-sequence)
  (:export #:connection #:threaded-connection #:netfarm-format-connection
           #:character-connection #:binary-connection
           #:passing-connection #:make-hidden-socket
           #:make-connection-listeners #:stop-connection
           #:connection-message-handler
           #:with-errors-blamed-on-connection
           #:read-message #:write-message
           #:protocol-creator #:define-protocol
           #:connection-from-uri
           #:connection-uri #:connection-uri-protocol-name
           #:connection-created-by-me-p
           #:write-block #:request-block #:request-blocks
           #:write-ok #:write-error
           #:announce-node #:allow-announcement
           #:connection-channels #:connection-destructors
           #:connection-id
           #:connection-stopped-p #:connection-announcing-p
           #:connection-element-type
	   #:connection-counter-value #:with-connection-counter
           #:connection-known-blocks #:add-known-block #:map-known-blocks
           #:subscribe #:write-subscription))
