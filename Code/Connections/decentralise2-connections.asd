(asdf:defsystem :decentralise2-connections
  :depends-on (:usocket :cl+ssl :dynamic-mixins :split-sequence
               :decentralise2-utilities :decentralise2-messages
               :safe-queue)
  :components ((:file "package")
               (:module "Protocol"
                :components ((:file "class")
                             (:file "messages")
                             (:file "element-types")
                             (:file "uri")))
               (:file "pair")
               (:file "socketed")
               (:file "define-syntax")
               (:file "netfarm-wire-format")))
