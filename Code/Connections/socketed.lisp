(in-package :decentralise-connection)

(defclass socketed-connection (threaded-connection)
  ((lock   :initform (bt:make-lock "SOCKETED-CONNECTION stream lock")
           :reader connection-lock)
   (stream :initarg :stream :reader connection-stream)
   (flush-buffer-every :initarg :flush-buffer-every :initform 1
                       :reader connection-flush-buffer-every)
   (unflushed-count :initform 0 :accessor connection-unflushed-count)
   (socket :initarg :socket :reader connection-socket)))

(defmethod read-message :around ((connection socketed-connection))
  (handler-case (call-next-method)
    (:no-error (message done?)
      (if done?
          (values nil t)
          (values message nil)))
    (end-of-file ()
      (stop-connection connection)
      (values nil t))))

(defmethod stop-connection ((connection socketed-connection))
  (ignore-errors
   (usocket:socket-close (connection-socket connection))))

(defgeneric connection-uri-protocol-name (connection)
  (:documentation "Return the protocol name used for a connection URI, such as \"netfarm\"."))

(defun format-ip (ip)
  (ecase (length ip)
    (4  (usocket:vector-quad-to-dotted-quad ip))
    (16 (usocket:vector-to-ipv6-host ip))))

(defmethod connection-uri ((connection socketed-connection))
  (format nil "~a:~a"
          (connection-uri-protocol-name connection)
          (format-ip
           (usocket:get-peer-address
            (connection-socket connection)))))

(defmethod write-message :after ((connection socketed-connection) message)
  (bt:with-lock-held ((connection-lock connection))
    (when (= (incf (connection-unflushed-count connection))
             (connection-flush-buffer-every connection))
      (setf (connection-unflushed-count connection) 0)
      (finish-output (connection-stream connection)))))
