(in-package :decentralise-connection)

(defclass threaded-connection (connection)
  ((listener)))

(defgeneric read-message (connection))

(defgeneric listener-loop (connection)
  (:method ((connection threaded-connection))
    (loop
      (multiple-value-bind (message done?)
          (read-message connection)
        (when done?
          (return-from listener-loop))
        (handle-message connection message)))))

(defmethod initialize-instance :after ((connection threaded-connection) &key)
  (setf (slot-value connection 'listener)
        (with-thread (:name (format nil "~s input listener"
                                    (class-name (class-of connection))))
          (listener-loop connection))))

(defmethod stop-connection ((connection threaded-connection))
  (setf (slot-value connection 'listener) nil))
