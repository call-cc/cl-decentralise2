(in-package :decentralise-kademlia)

;;; The Kademlia client is quite big, possibly because we attempt to query
;;; subclients concurrently and with some fault tolerance.

;;; We store some information about both URIs we can try to connect to later,
;;; and extra information about clients, such as what URI we used to create it.
(defclass uri-information ()
  ((uri :initarg :uri
        :initform (alexandria:required-argument :uri)
        :reader uri-information-uri)
   (hash :initarg :hash
         :initform (alexandria:required-argument :hash)
         :reader uri-information-hash)))
(defmethod print-object ((uri uri-information) stream)
  (print-unreadable-object (uri stream :type t)
    (write-string (uri-information-uri uri) stream)))

(defclass client-information ()
  ((client :initarg :client
           :initform (alexandria:required-argument :client)
           :reader client-information-client)
   (uri    :initarg :uri
           :initform (alexandria:required-argument :uri)
           :reader client-information-uri)
   (requests :initform 0
             :accessor client-information-requests)
   (successful-requests :initform 0
                        :accessor client-information-successful-requests)))

(defclass kademlia-client (decentralise-client:client kademlia-mixin)
  ((clients :initform (box '() :copier #'copy-list)
            :accessor %kademlia-client-clients
            :documentation "The current subclient information objects of this client.")
   (concurrent-requests :initarg :concurrent-requests
                        :initform 3
                        :reader kademlia-client-concurrent-requests
                        :documentation "The number of requests this client can make at a time.")
   (current-searches :initform (box '())
                     :reader %kademlia-client-current-searches)
   (known-uris :initform (box '() :copier #'copy-list)
               :reader %kademlia-client-known-uris
               :documentation "A box with the URIs that this client can retrieve later.")
   (subclient-timeout :initarg :subclient-timeout
                      :initform 10
                      :reader kademlia-client-subclient-timeout
                      :documentation "The time, in seconds, that the client will wait for a subclient to respond.")
   (subclient-class :initarg :subclient-class
                    :initform 'decentralise-client:connection-client
                    :reader kademlia-client-subclient-class
                    :documentation "The class of subclients that the client should create.") 
   (scheduler :initform (make-instance 'scheduler)
              :reader kademlia-client-scheduler)
   (searcher :reader kademlia-client-searcher))
  (:documentation "A client that can find subclients and retrieve blocks from systems that subclass KADEMLIA-SYSTEM."))

(defmethod initialize-instance :after ((client kademlia-client)
                                       &key (worker-threads 1)
                                            (threads-per-search 3)
                                            (maximum-search-threads 15)
                                            (bootstrap-uris '()))
  (setf (slot-value client 'searcher)
        (make-instance 'searcher
                       :client client
                       :threads-per-search threads-per-search
                       :maximum-threads maximum-search-threads))
  (start-scheduler (kademlia-client-scheduler client)
                   :threads worker-threads)
  (dolist (uri bootstrap-uris)
    (add-subclient client uri)))

(defmethod decentralise-client:stop-client ((client kademlia-client))
  (stop-scheduler (kademlia-client-scheduler client)))

;;; Box accessor macros. Maybe these should go in Utilities/
(defmacro define-with-box (with-name accessor-name box-value-name)
  `(defmacro ,with-name ((,box-value-name client) &body body)
     `(decentralise-utilities:with-unlocked-box
          (,,box-value-name (,',accessor-name ,client))
        ,@body)))
(defmacro with-box-value ((box) &body body)
  `(decentralise-utilities:with-unlocked-box (,box ,box)
     ,@body))
(define-with-box with-subclients %kademlia-client-clients clients)
(define-with-box with-known-uris %kademlia-client-known-uris uris)

(defun kademlia-client-subclients (client)
  (box-value (%kademlia-client-clients client)))
(defun kademlia-client-known-uris (client)
  (box-value (%kademlia-client-known-uris client)))

(defmethod decentralise-client:client-has-block-p
    ((client kademlia-client) name)
  (loop for subclient in (kademlia-client-subclients client)
        thereis (decentralise-client:client-has-block-p name)))

(defgeneric add-subclient-information (client uri subclient)
  (:method ((client kademlia-client) uri subclient)
    (let* ((node-data (decentralise-client:get-data subclient "nodes"
                                                    :timeout 10))
           (information (make-instance 'client-information
                                       :client subclient
                                       :uri (uri-information-uri uri))))
      (with-subclients (clients client)
        (push information clients)
        (with-known-uris (uris client)
          (setf uris (remove uri uris)))
        (map-nodes (lambda (uri id)
                     (let ((hash (parse-hash-name client id)))
                       (assert (not (null hash)))
                       (with-known-uris (uris client)
                         (unless (or (member uri uris
                                             :key #'uri-information-uri
                                             :test #'string=)
                                     (member uri clients
                                             :key #'client-information-uri
                                             :test #'string=))
                           (let ((uri (make-instance 'uri-information
                                                     :hash hash
                                                     :uri uri)))
                             (push uri uris)
                             (with-unlocked-box (jobs (%kademlia-client-current-searches client))
                               (dolist (job jobs)
                                 (add-node-information-to-job (kademlia-client-searcher client)
                                                              job uri))))))))
                   node-data)))))

(defgeneric add-subclient (client uri)
  (:method ((client kademlia-client) uri)
    (let ((subclient (decentralise-client:connect-to-uri uri
                      :should-get-system-info t
                      :class (kademlia-client-subclient-class client))))
      (add-subclient-information client (make-instance 'uri-information
                                                       :uri uri
                                                       :hash nil)
                                 subclient))))

(defgeneric sort-known-uris (client hash)
  (:method ((client kademlia-client) hash)
    (let ((uris (kademlia-client-known-uris client)))
      (sort uris #'<
            :key (lambda (uri)
                   (logxor hash (uri-information-hash uri)))))))

(defgeneric compute-applicable-subclients (client name)
  (:method ((client kademlia-client) name)
    (with-subclients (clients client)
      (loop for client-information in clients
            for client = (client-information-client client-information)
            for version = (decentralise-client:client-has-block-p client name)
            unless (null version)
              collect (cons client version) into clients
            finally (return (mapcar #'car
                                    (sort clients #'> :key #'cdr)))))))

(defclass request (decentralise-client:request)
  ((mailbox :initarg :mailbox :reader request-mailbox)
   (value   :initform nil :accessor %request-value)))
(defmethod decentralise-client:request-value ((request request) &key timeout)
  (flet ((handle-value (value)
           (if (car value)
               (return-from decentralise-client:request-value (cdr value))
               (error (cdr value)))))
    (unless (null (%request-value request))
      (handle-value (%request-value request)))
    (let ((value (safe-queue:mailbox-receive-message
                  (request-mailbox request)
                  :timeout timeout)))
      (when (null value)
        (error 'request-timeout))
      (setf (%request-value request) value)
      (handle-value value))))

(defun %map-subclients (function client subclients %succeed %fail give-up)
  (let* ((done? (box nil))
         (subclients (safe-queue:make-queue :initial-contents subclients))
         (searchers (min (safe-queue:queue-count subclients)
                         (kademlia-client-concurrent-requests client)))
         (remaining-searchers (box searchers)))
    (labels ((handle-end-of-search ()
               ;; Only give up when no other searchers are running.
               (with-box-value (remaining-searchers)
                 (decf remaining-searchers)
                 (when (zerop remaining-searchers)
                   (call-after (kademlia-client-scheduler client) 0 give-up))))
             (fail (condition)
               (with-box-value (done?)
                 (setf done? t))
               (funcall %fail condition))
             (succeed (value)
               (with-box-value (done?)
                 (setf done? t))
               (funcall %succeed value))
             (work-step ()
               (with-box-value (done?)
                 (when done?
                   (return-from work-step)))
               (when (safe-queue:queue-empty-p subclients)
                 (handle-end-of-search)
                 (return-from work-step))
               (multiple-value-bind (subclient subclient?)
                   (safe-queue:dequeue subclients)
                 (unless subclient?
                   (return-from work-step (handle-end-of-search)))
                 (handler-bind ((error #'fail))
                   (funcall function subclient #'succeed #'fail))
                 (call-after (kademlia-client-scheduler client)
                             (kademlia-client-subclient-timeout client)
                             #'work-step))))
      (when (zerop searchers)
        (handle-end-of-search))
      (dotimes (n searchers)
        (call-after (kademlia-client-scheduler client) 0 #'work-step)))))

(defmethod decentralise-client:map-subclients
    (function (client kademlia-client) message-name)
  (let ((name (second message-name))
        (mailbox (safe-queue:make-mailbox))
        (attempts 3))
    (labels ((finish-with (success? value)
               (safe-queue:mailbox-send-message mailbox
                                                (cons success? value)))
             (succeed (value)  (finish-with t   value))
             (fail (condition)
               (finish-with nil condition))
             (not-found ()
               (fail (make-condition 'decentralise-client:remote-error
                                     :name name :message "not found")))
             (ensure-client ()
               (if (zerop attempts)
                   (not-found)
                   (progn
                     (decf attempts)
                     (do-after ((kademlia-client-scheduler client) 0)
                       (ensure-client-with-name-is-reachable
                        client name
                        #'actually-map-subclients
                        #'fail)))))
             (actually-map-subclients ()
               (%map-subclients function client
                                (compute-applicable-subclients client name)
                                #'succeed #'fail #'ensure-client)))
      (call-after (kademlia-client-scheduler client) 0 #'ensure-client)
      (make-instance 'request :mailbox mailbox))))
