(in-package :decentralize2-setup)

#|
Simple functions that allow you to easily conjure the spirits of the computer.
|# 

(defmacro make-acceptor (name &rest arguments)
  `(make-instance ',name
                  ,@(loop for (key value) on arguments by #'cddr
                          appending `(,key ',value)))) 

(defmacro setup-wizard-defaults (class-name instance-name key cert)
  `(progn
     (defclass ,class-name (decentralise-standard-system:standard-system
			       decentralise-system:memory-database-mixin)
       ())
     (defvar ,instance-name (make-instance ',class-name
					   :acceptors (list
						       (make-instance 'decentralise-acceptors:socket-acceptor
								      :connection-class 'decentralise-connections:netfarm-format-connection
								      :key ,key
								      :certificate ,cert))))))


(defmacro setup-wizard ((&rest classes) &rest arguments &key acceptors &allow-other-keys)
  `(make-instance
    (make-instance 'standard-class
		   :direct-superclasses (list ,@(loop for class in classes
						      collect `(find-class ',class))))
    :acceptors (list ,@(loop for acceptor-specs in acceptors
                           collect `(make-acceptor ,@acceptor-specs)))
    ,@(alexandria:remove-from-plist arguments :acceptors)))
