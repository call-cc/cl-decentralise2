(in-package :decentralise-client)

(defclass client () ()
  (:documentation "A protocol class for a client, which can be used to send and receive messages from a system synchronously."))

(defgeneric client-connection (client)
  (:method ((client client)) nil)
  (:documentation "The connection associated with the client if it has one, or NIL."))

(defgeneric stop-client (client))
(defgeneric call-with-consistent-state (client function)
  (:documentation "Call function while CLIENT has consistent state. This is used to implement WITH-CONSISTENT-STATE."))
(defmacro with-consistent-state ((client) &body body)
  `(call-with-consistent-state ,client (lambda () ,@body)))

(defgeneric message-name (client message)
  (:method ((client client) message)
    (declare (ignore message))
    :unknown))
(macrolet ((define-message (keyword type accessor)
             `(defmethod message-name ((client client) (message ,type))
                (list ',keyword (,accessor message)))))
  ;; Define message-name methods for the inbuilt messages we expect to receive.
  (define-message :block decentralise-messages:put-block
    decentralise-messages:put-block-name)
  (define-message :ok decentralise-messages:ok-response
    decentralise-messages:ok-response-name)
  (define-message :error decentralise-messages:error-response
    decentralise-messages:error-response-name))

(defgeneric condition-from-message (client message)
  (:argument-precedence-order message client)
  (:method (client (message decentralise-messages:error-response))
    (make-condition 'remote-error
                    :name (decentralise-messages:error-response-name message)
                    :message (decentralise-messages:error-response-reason message))))

(defgeneric client-has-block-p (client name))
(defgeneric update-known-block-info (client data))

(defgeneric client-subscriptions (client))
(defgeneric (setf client-subscriptions) (new-subscriptions client))

(defgeneric handle-message (client message))
(defgeneric add-handler (client message-name function))
(defgeneric send-message (client message))
(defgeneric map-subclients (function client message-name)
  (:documentation "Call FUNCTION repeatedly with each applicable subclient, and success and failure callbacks.
Returns a request object that represents the operation in progress."))
(defmacro do-subclients ((subclient client message-name) &body body)
  (alexandria:with-gensyms (success failure)
    `(map-subclients (lambda (,subclient ,success ,failure)
                       (flet ((succeed (value)
                                (funcall ,success value))
                              (fail (condition)
                                (funcall ,failure condition)))
                         (declare (ignorable #'succeed #'fail))
                         (with-consistent-state (,subclient)
                           ,@body)))
                     ,client ,message-name)))

(defclass request ()
  ((client :initarg :client :reader request-client))
  (:documentation "A protocol class for a request, which is returned from SEND-MESSAGE-AND-HANDLE."))

(defgeneric request-value (request &key timeout &allow-other-keys)
  (:documentation "Get the block retrieved by the request, either by returning it if it had a success name, or signalling an appropriate condition if it had an error name."))
(defgeneric give-up-on-request (request)
  (:documentation "Give up on a request, allowing a client to remove some information about it.")
  (:method ((request request))
    "The default method does nothing."
    (values)))
