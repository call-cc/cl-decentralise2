(defpackage :decentralise-client
  (:use :cl :decentralise-utilities)
  (:export #:client #:connection-client
           #:connect-to-uri
	   #:connect-to-system
           #:stop-client
           #:call-with-consistent-state #:with-consistent-state
           #:message-name #:signal-error
           #:add-handler #:send-message #:map-subclients
           #:do-subclients #:succeed #:fail
           #:get-block #:get-block* #:get-data #:put-block #:put-block*
           #:request #:request-value
           #:client-subscriptions
           #:client-has-block-p
           #:client-connection
           #:connection-closed #:remote-error #:request-timeout
           #:remote-error-message #:remote-error-name #:remote-error-case))
