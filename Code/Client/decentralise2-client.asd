(asdf:defsystem :decentralise2-client
  :depends-on (:decentralise2-connections :decentralise2-systems :safe-queue)
  :components ((:file "package")
               (:file "conditions")
               (:file "protocol")
               (:file "connection-client")
               (:file "client")
               (:file "direct-system")))
