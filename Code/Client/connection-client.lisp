(in-package :decentralise-client)

(defclass connection-client (client)
  ((connection :initarg :connection :reader client-connection)
   (handler-table :initform (make-hash-table :test 'equal) :reader client-handler-table)
   (known-blocks :initform (make-hash-table :test 'equal) :reader client-known-blocks)
   (state-lock :initform (bt:make-recursive-lock "client table lock") :reader client-state-lock)
   (subscriptions :initform '() :accessor client-subscriptions)
   (should-get-system-info :initarg :should-get-system-info :initform t
                           :reader client-should-get-system-info))
  (:documentation "A client that can take synchronous requests on behalf of a connection.
The connection used when instantiating a client should not be accessed by anything other than that client."))

(defmethod client-has-block-p ((client connection-client) name)
  (with-consistent-state (client)
    (cond
      ((not (client-should-get-system-info client)) nil)
      ((null (client-known-blocks client)) nil)
      (t (values (gethash name (client-known-blocks client)))))))

(defmethod update-known-block-info ((client connection-client) data)
  (flet ((update-known-block (name version channels)
           (declare (ignore channels))
           (setf (gethash name (client-known-blocks client))
                 version)))
    (if (stringp data)
        (map-listing #'update-known-block data)
        (map-binary-listing #'update-known-block data))))

(defmethod (setf client-subscriptions) :before (new-subscriptions (client connection-client))
  "If we aren't listening for everything (using the channel all), then send over the set of channels designated by the client's subcription list."
  (unless (client-should-get-system-info client)
    (let ((new-subscriptions '()))
      (dolist (pair new-subscriptions)
        (destructuring-bind (function &rest channels) pair
          (declare (ignore function))
          (setf new-subscriptions (union channels new-subscriptions :test #'string=))))
      (decentralise-connection:subscribe (client-connection client) new-subscriptions))))

(defvar *in-consistent-state* '())
(defmethod call-with-consistent-state ((client connection-client) function)
  (bt:with-recursive-lock-held ((client-state-lock client))
    (let ((*in-consistent-state* (cons client *in-consistent-state*)))
      (funcall function))))

(defmethod handle-message ((client connection-client) message)
  (let ((name (message-name client message)))
    (with-consistent-state (client)
      (let ((callbacks (gethash name (client-handler-table client))))
        (remhash name (client-handler-table client))
        (loop for callback in callbacks
              do (funcall callback message))))))

(defclass simple-request (request)
  ((mailbox :initarg :mailbox :reader simple-request-mailbox)
   (value   :initform nil :accessor simple-request-value)))

(defmethod add-handler :before ((client connection-client) message-name function)
  (assert (member client *in-consistent-state*) ()
          "You do not have consistent state of ~s" client))
(defmethod add-handler ((client connection-client) message-name function)
  (push function (gethash message-name (client-handler-table client)))
  (values))
    
(defmethod send-message ((client connection-client) message)
  (decentralise-connection:write-message (client-connection client)
                                         message))
(defmethod map-subclients
    (function (client connection-client) message-name)
  (let* ((mailbox (safe-queue:make-mailbox))
         (request (make-instance 'simple-request
                                 :mailbox mailbox :client client)))
    (flet ((succeed (value)
             (safe-queue:mailbox-send-message mailbox
                                              (cons t value)))
           (fail (condition)
             (safe-queue:mailbox-send-message mailbox
                                              (cons nil condition))))
      (handler-bind ((error #'fail))
        (funcall function client #'succeed #'fail))
      request)))

(defmethod request-value ((request simple-request) &key timeout)
  (flet ((handle-value (value)
           (if (car value)
               (return-from request-value (cdr value))
               (error (cdr value)))))
    (unless (null (simple-request-value request))
      (handle-value (simple-request-value request)))
    (let ((value (safe-queue:mailbox-receive-message
                  (simple-request-mailbox request)
                  :timeout timeout)))
      (when (null value)
        (error 'request-timeout))
      (setf (simple-request-value request) value)
      (handle-value value))))

(defgeneric glean-information-from-message (client message)
  (:method ((client connection-client) unknown)
    (declare (ignore unknown)))
  (:method ((client connection-client) (put decentralise-messages:put-block))
    (decentralise-messages:message-case put
      ((:block name ~ ~ data)
       (alexandria:switch (name :test #'string=)
         ("list" (update-known-block-info client data))))))
  (:method ((client connection-client) (subscription decentralise-messages:subscription))
    (decentralise-messages:message-case subscription
      ((:subscription name version ~)
       (setf (gethash name (client-known-blocks client)) version)))))

(defmethod initialize-instance :after ((client connection-client) &key)
  (let ((connection (client-connection client)))
    (setf (decentralise-connection:connection-message-handler connection)
          (lambda (message)
            (glean-information-from-message client message)
            (handle-message client message)))
    (setf (decentralise-connection:connection-id connection)
          (parse-integer (get-data client "id") :radix 16))
    (when (client-should-get-system-info client)
      (decentralise-connection:subscribe (client-connection client)
                                         '("all"))
      (get-data client "list" :timeout 10))))

(defmethod stop-client ((client connection-client))
  (decentralise-connection:stop-connection (client-connection client)))
