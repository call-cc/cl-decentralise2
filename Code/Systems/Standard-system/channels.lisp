(in-package :decentralise-standard-system)

(defgeneric connection-interested-in-p (system connection name version channels)
  (:method ((system standard-system) connection name version channels)
    (declare (ignore name version))
    (let ((conn-channels (decentralise-connection:connection-channels connection)))
      (or (member "all" conn-channels :test #'string=)
          (loop for channel in channels
                  thereis (member channel conn-channels :test #'string=))))))

(defgeneric broadcast-metadata (system name version channels)
  (:documentation "Broadcast metadata about a new object to all interested connections.")
  (:method ((system standard-system) name version channels)
    (with-unlocked-box (connections (decentralise-system:system-connections system))
      (dolist (connection connections)
        (when (connection-interested-in-p system connection name version channels)
          (decentralise-connection:write-subscription connection name version channels))))))

(defgeneric distribute-after-put-block (system name version channels)
  (:method ((system standard-system) name version channels)
    (broadcast-metadata system name version channels)))
