(in-package :decentralise-standard-system)

(define-simple-error invalid-listing)
(define-simple-error too-old)

(defstruct character-listing-data text)
(defstruct binary-listing-data bytes)
(defgeneric map-listing-data (function listing-data)
  (:method (function (listing character-listing-data))
    (map-listing function (character-listing-data-text listing)))
  (:method (function (listing binary-listing-data))
    (map-binary-listing function (binary-listing-data-bytes listing))))
(defmethod data->object (data (connection decentralise-connection:binary-connection)
                         (type (eql 'listing)))
  (make-binary-listing-data :bytes data))
(defmethod data->object (data (connection decentralise-connection:character-connection)
                         (type (eql 'listing)))
  (make-character-listing-data :text data))

(defstruct listing system)
(defmethod object->data ((listing listing) (connection decentralise-connection:character-connection)
                         source)
  (declare (ignore source))
  (with-output-to-string (buffer)
    (decentralise-system:map-blocks
     (lambda (name version channels)
       (write-listing-line name version channels buffer)) 
     (listing-system listing))))
(defmethod object->data ((listing listing) (connection decentralise-connection:binary-connection)
                         source)
  (declare (ignore source))
  (with-output-to-byte-array (consumer)
    (decentralise-system:map-blocks
     (lambda (name version channels)
       (write-listing-data name version channels #'consumer))
     (listing-system listing))))

(define-special-block (standard-system "list" listing)
  :get (lambda (system connection)
	 (decentralise-connection:with-connection-counter (n connection list)
	   (values n '() (make-listing :system system))))
  :put (lambda (system connection version channels data)
         (declare (ignore version channels))
	 (bt:with-lock-held ((system-known-blocks-lock system))
	   (handler-case
               (map-listing-data
                (lambda (name version channels)
                  (%add-known-block system connection
                                    name version channels)
                  (decentralise-connection:add-known-block connection
                                                           name
                                                           version
                                                           channels))
                data)
	     (error ()
               (error 'invalid-listing))))))
