(defpackage :decentralise-standard-system
  (:use :cl :decentralise-utilities :decentralise-messages)
  (:export #:standard-system
           #:define-simple-error #:define-special-block
           #:update-system-for-new-interesting-block-predicate
           #:interesting-block-p
           #:connection-interested-in-p #:broadcast-metadata
           #:distribute-after-put-block
           #:too-old))
