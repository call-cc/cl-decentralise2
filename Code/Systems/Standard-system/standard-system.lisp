(in-package :decentralise-standard-system)

(declaim (optimize (speed 3) (safety 1) (debug 1)))
(defun make-block-table ()
  (make-hash-table :test 'equal
		   :rehash-size 2.0))

(defclass standard-system (decentralise-system:system)
  ((scheduler-timeout :initarg :timeout :initform 10 :reader scheduler-timeout)
   (scheduler-concurrent-requests :initarg :concurrent-requests
                                  :initform 80 :reader scheduler-requests)
   (current-requests :initform (make-block-table)
                     :accessor scheduler-current-requests)
   (block-consistency-locks :initform (decentralise-utilities:make-locked-box
                                       :value (make-hash-table :test 'equal))
                            :reader block-consistency-locks)
   (interesting-blocks-queue :initform (safe-queue:make-queue)
                             :reader system-interesting-blocks-queue)
   (interesting-blocks :initform (make-block-table)
                       :reader system-interesting-blocks)
   (uninteresting-blocks :initform (make-block-table)
                         :reader system-uninteresting-blocks)
   (known-blocks-lock :initform (bt:make-lock "Standard-System block information lock")
                      :reader system-known-blocks-lock)))

(defgeneric interesting-block-p (system name version channels)
  (:documentation "From an object's metadata (NAME, VERSION and CHANNELS), 
determine if it could be of use to the system.")
  (:method ((system standard-system) name version channels)
    "The default method considers all objects interesting."
    (declare (ignore name version channels))
    t))

(defmethod decentralise-system:leader-loop ((system standard-system))
  (loop
    (when (decentralise-system:system-shutdown-request system)
      (return))
    (bt:with-lock-held ((system-known-blocks-lock system))
      (remove-old-requests system)
      (loop while (should-add-requests-p system)
            do (add-generated-request system)))
    (sleep 0.1)))

(defun standard-system-message-dispatcher (connection system message)
  (decentralise-messages:message-case message
    ((:block name version channels data)
     (bt:with-lock-held ((system-known-blocks-lock system))
       (remove-request system name)
       (add-generated-request system))
     (handler-case* (special-put system connection
                                 (find-special-block-name name)
                                 name version channels data)
       (error (e)
         (decentralise-connection:write-error
          connection name
          (decentralise-system:describe-decentralise-error e)))
       (:no-error (&rest r)
         (declare (ignore r))
         (decentralise-connection:write-ok connection name))))
    ((:get names)
     (loop for name in names
           do (handler-case* (special-get system connection
                                          (find-special-block-name name)
                                          name)
                (error (e)
                   (decentralise-connection:write-error
                    connection name
                    (decentralise-system:describe-decentralise-error e)))
                (:no-error (version channels data)
                   (decentralise-connection:write-block connection name version
                                                        channels data)))))
    ((:allow-announcement announce-p)
     (if (and announce-p (null (decentralise-connection:connection-id connection)))
         (decentralise-connection:write-error connection "announce" "need id first")
         (setf (decentralise-connection:connection-announcing-p connection)
               announce-p)))
    ((:subscribe subscriptions)
     (setf (decentralise-connection:connection-channels connection)
           subscriptions))
    ((:subscription name version channels)
     (add-known-block system connection name version channels)
     (decentralise-connection:add-known-block connection
                                              name version channels))))

(defmethod decentralise-system:add-connection :after
    ((system standard-system) connection)
  (setf (decentralise-connection:connection-message-handler connection)
        (lambda (message)
          (standard-system-message-dispatcher connection system message)))
  (push (lambda (connection)
          (remove-connection-info system connection))
        (decentralise-connection:connection-destructors connection)))
