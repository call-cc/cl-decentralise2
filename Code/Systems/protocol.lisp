(in-package :decentralise-system)

(defclass system ()
  ((acceptors   :initform '() :initarg :acceptors :accessor system-acceptors)
   (leader-thread :accessor system-leader-thread)
   (id :initform (random (load-time-value (expt 2 256)))
       :initarg :id
       :reader system-id)
   (shutdown    :initform nil :accessor system-shutdown-request)
   (backend-lock :initform (bt:make-lock "system backend lock")
		 :reader system-backend-lock)
   (connections :initform (make-locked-box :value '())
                :reader system-connections)))
(defmethod decentralise-messages:data->object (data source (target system))
  data)
(defmethod decentralise-acceptor:give-system-connection
    ((system system) connection)
  (add-connection system connection))

(define-condition decentralise-error (error) ())
(define-condition not-found (decentralise-error)
  ((object :initarg :name :reader not-found-object-name))
  (:report (lambda (condition stream)
             (format stream "Could not find the object named ~s"
                     (not-found-object-name condition)))))

(defgeneric put-block (system name version channels data)
  (:documentation "Attempt to put an object (with name NAME, version VERSION, etc) to the system SYSTEM. If the object cannot be put, an error should be signalled, and it will be reported to the client using the description generated by DESCRIBE-DECENTRALISE-ERROR."))

(defgeneric get-block (system name)
  (:documentation "Get an object by returning the values VERSION, CHANNELS and DATA, or signal an error. The error will be reported to the client using the description generated by DESCRIBE-DECENTRALISE-ERROR."))

(defgeneric get-block-metadata (system name)
  (:documentation "Get an object's metadata by returning the values VERSION and CHANNELS, or signal an error.")
  (:method ((system system) name)
    "The default method proxies GET-OBJECT, allowing a slack database designer to forget about GET-OBJECT-METADATA until they realise they probably should have included that too."
    (multiple-value-bind (version channels data)
        (get-block system name)
      (declare (ignore data))
      (values version channels))))

(defgeneric map-blocks (function system)
  (:documentation "Repeatedly call FUNCTION with each object's name, version and channels."))

(defgeneric leader-loop (system)
  (:documentation "The loop that the leader thread runs. This should do something with events from connections."))

(defgeneric add-connection (system connection)
  (:documentation "Add a connection to the system's connection list, and create a thread to manage it.")
  (:method :before ((system system) connection)
    (push (lambda (connection)
            (with-unlocked-box (connections (system-connections system))
              (setf connections (remove connection connections))))
          (decentralise-connection:connection-destructors connection)))
  (:method ((system system) connection)
    (with-unlocked-box (connections (system-connections system))
      (push connection connections))))

(defgeneric start-system (system)
  (:documentation "Start a system, by starting all its acceptors and a leader thread, and setting SYSTEM-SHUTDOWN-REQUEST to NIL.")
  (:method ((system system))
    (dolist (acceptor (system-acceptors system))
      (decentralise-acceptor:start-acceptor acceptor system))
    (setf (system-shutdown-request system) nil
          (system-leader-thread system)
          (with-thread (:name (format nil "~a leader thread" (type-of system)))
            (leader-loop system)))))

(defgeneric describe-decentralise-error (error)
  (:documentation "Provide a description for an error to be sent to a peer.")
  (:method ((error error))
    (trivial-backtrace:print-backtrace error)
    "internal error")
  (:method ((not-found not-found))
    "not found"))

(defgeneric stop-system (system)
  (:documentation "Stop a system by closing all its acceptors and connections, and setting SYSTEM-SHUTDOWN-REQUEST to T.")
  (:method ((system system))
    (setf (system-shutdown-request system) t)
    (let (connections)
      (with-unlocked-box (connections* (system-connections system))
        (setf connections connections*))
      (mapc #'decentralise-connection:stop-connection connections))
    (dolist (acceptor (system-acceptors system))
      (ignore-errors
       (decentralise-acceptor:stop-acceptor acceptor)))
    t))

(defgeneric connect-system (system uri-or-connection)
  (:documentation "Connect a system to another node by URI or connection")
  (:method ((system system) (uri string))
    (let ((connection (decentralise-connection:connection-from-uri uri
                                                                   :synchronising t
                                                                   :created-by-me t)))
      (connect-system system connection)))
  (:method ((system system) connection)
    (setf (decentralise-connection:connection-id connection)
          (system-id system))
    (add-connection system connection)
    ;; This ordering is very important. If the next two lines were switched,
    ;; then new blocks could appear between the listing and subscription and
    ;; there would be a semi-permanent gap in system knowledge.
    (decentralise-connection:subscribe connection '("all"))
    (decentralise-connection:request-block connection "list")
    (decentralise-connection:request-block connection "id")
    connection))

(defun new-version-p (system name version)
  "A helper function to determine if new object metadata indicates it should replace the currently held object in SYSTEM."
  (handler-case (get-block-metadata system name)
    (not-found ()
      (not (minusp version)))
    (:no-error (our-version channels)
      (declare (ignore channels))
      (> version our-version))))
