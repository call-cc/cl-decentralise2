(asdf:defsystem :decentralise2-systems
  :depends-on (:decentralise2-utilities :decentralise2-connections
               :decentralise2-acceptors
               :alexandria :trivial-backtrace)
  :components ((:file "package")
               (:file "protocol")
               (:file "echo")
               (:file "memory-database-mixin")
               (:file "locks")
               (:module "Standard-system"
                :components ((:file "package")
                             (:file "block-consistency")
                             (:file "standard-system")
                             (:file "special-blocks")
                             (:file "known-blocks")
                             (:file "requests")
                             (:file "channels")
                             (:module "Inbuilt-special-blocks"
                              :components ((:file "list")
                                           (:file "id")))))))
