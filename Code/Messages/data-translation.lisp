(in-package :decentralise-messages)

(defgeneric object->data (object target source))

(defgeneric data->object (data source target)
  (:method (data source target)
    data))
