(in-package :decentralise-messages)

;;; A database of message keyword "designators", constructors and accessors.

(defstruct message-type type-specifier constructor accessors)
(defvar *message-types* (make-hash-table))

(defun message-type (keyword)
  (let ((message-type (gethash keyword *message-types*)))
    (if (null message-type)
        (error "~s does not designate a message type" keyword)
        message-type)))

(defun message-type-specifier (keyword)
  (message-type-type-specifier (message-type keyword)))
(defun message-accessors (keyword)
  (message-type-accessors (message-type keyword)))
(defun message-constructor (keyword)
  (message-type-constructor (message-type keyword)))

(defmacro define-message-type (keyword type-specifier constructor &rest accessors)
  (check-type keyword symbol)
  `(setf (gethash ',keyword *message-types*)
         (make-message-type :accessors ',accessors
                            :constructor ',constructor
                            :type-specifier ',type-specifier)))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun make-bindings (keyword message parts message-type)
    (let ((accessors (message-type-accessors message-type)))
      (assert (= (length parts) (length accessors)) ()
              "The pattern (~s~{ ~s~}) has ~d value~:p but the message type ~s only has ~d value~:p"
              keyword parts (length parts) keyword (length accessors))
      (loop for part in parts
            for accessor in accessors
            do (check-type part symbol)
            unless (or (null part)
                       (member (symbol-name part) '("_" "-" "~") :test #'string=))
              collect `(,part (,accessor ,message))))))

(defmacro message-case (message &body cases)
  (alexandria:once-only (message)
    `(typecase ,message
       ,@(loop for ((keyword . parts) . body) in cases
               for type = (gethash keyword *message-types*)
               do (assert (not (null type)) ()
                          "~s does not designate a message type" keyword)
               collect `(,(message-type-type-specifier type)
                         (let ,(make-bindings keyword message parts type)
                           ,@body))))))

(defun message (keyword &rest arguments)
  (apply (message-constructor keyword) arguments))

(define-compiler-macro message (&whole w keyword &rest arguments)
  (if (keywordp keyword)
      (handler-case
          `(,(message-constructor keyword) ,@arguments)
        (error (e) w))
      w))
