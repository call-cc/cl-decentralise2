(asdf:defsystem :decentralise2-acceptors
  :depends-on (:decentralise2-connections :decentralise2-utilities
               :usocket :cl+ssl)
  :components ((:file "package")
               (:file "protocol")
               (:file "socketed")))
