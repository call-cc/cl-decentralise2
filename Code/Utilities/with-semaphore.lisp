(in-package :decentralise-utilities)

(defmacro with-semaphore ((semaphore &key timeout) &body body)
  (alexandria:once-only (semaphore)
  `(progn
     (bt:wait-on-semaphore ,semaphore :timeout ,timeout)
     (unwind-protect
          (progn ,@body)
       (bt:signal-semaphore ,semaphore)))))
