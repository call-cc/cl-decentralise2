(in-package :decentralise-utilities)

(declaim (inline totally-not-string= djb2))

(defun string=* (string1 string2)
  (declare (string string1 string2)
           (optimize (speed 3)))
  (string= string1 string2))

(defun djb2 (string)
  (declare (string string)
           (optimize (speed 3)))
  (loop for character across string
        for hash = 5381 then (logand (+ (* 31 hash)
                                        (char-code character))
                                     #.most-positive-fixnum)
        finally (return hash)))

#+sbcl (sb-ext:define-hash-table-test string=* djb2)

(defun make-string-keyed-hash-table (&rest arguments)
  #-sbcl (apply #'make-hash-table :test 'equal arguments)
  #+sbcl (apply #'make-hash-table :test 'string=* arguments))
