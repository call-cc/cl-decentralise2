(in-package :decentralise-utilities)
(defstruct locked-box
  value
  (lock (bt:make-lock "locked-box lock") :read-only t)
  (copier nil :read-only t))

(defmacro with-unlocked-box ((value box) &body body)
  "An attempt at some sort of thread safety trick. The idea is that we
can only write to the locked value inside this macro body. It might work.
Maybe.

  (defvar *box* (make-locked-box :value 42))

  (with-thread (:name \"Evil fighting thread!\")
    (loop
      (setf v :woohoo-type-error-time)
      (sleep 0.01)))

  (with-unlocked-box (v *box*)
    (setf v 12300)
    (+ v 45)) ; => always 12345
"
  (alexandria:once-only (box)
    `(bt:with-lock-held ((locked-box-lock ,box))
       (symbol-macrolet ((,value (locked-box-value ,box)))
         . ,body))))

;;; Having values escape the dynamic extent of the locked code is probably a
;;; terrible idea usually, but makes sense if we can copy it and return an
;;; "equivalent" value.
(defun box-value (box)
  (if (null (locked-box-copier box))
      (error "Can't get the value of ~s" box)
      (with-unlocked-box (value box)
        (funcall (locked-box-copier box) value))))

(defun box (initial-value &key copier)
  (make-locked-box :value initial-value
                   :copier copier))
