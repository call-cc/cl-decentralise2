(in-package :decentralise-tests)

(defvar *systems* (make-hash-table :test 'equalp))

(defclass test-passing-connection (decentralise-connection:passing-connection)
  ((system :initarg :system :accessor test-passing-connection-system)))
   
(defmethod decentralise-connection:connection-uri ((connection test-passing-connection))
  (format nil "test:~a"
          (test-system-name
           (test-passing-connection-system
            (decentralise-connection::passing-connection-target connection)))))

(defclass test-system (standard-system memory-database-mixin)
  ((name :initarg :name :initform nil :reader test-system-name)))
(defmethod initialize-instance :after ((system test-system) &key)
  (unless (null (test-system-name system))
    (setf (gethash (test-system-name system) *systems*) system)))

(decentralise-connection:define-protocol test
  (lambda (hostname)
    (let ((system (gethash hostname *systems*)))
      (when (null system)
        (error "no test system named ~s" hostname))
      (multiple-value-bind (connection1 connection2)
          (decentralise-connection:make-hidden-socket :class 'test-passing-connection
                                                       :initargs (list :system system))
        (decentralise-system:add-connection system connection1)
        connection2))))

(defun attach-systems (system1 system2 &key (class 'test-passing-connection))
  (multiple-value-bind (connection1 connection2)
      (make-hidden-socket :class class :name "system glue")
    (add-connection system1 connection1)
    (add-connection system2 connection2)
    (setf (test-passing-connection-system connection1) system1)
    (setf (test-passing-connection-system connection2) system2)
    (write-block connection1 "id" 1 '() (render-id (system-id system1)))
    (write-block connection2 "id" 1 '() (render-id (system-id system2)))
    (allow-announcement connection1 t)
    (allow-announcement connection2 t)
    (values connection1 connection2)))

(defmacro define-network ((name &key
                                (start-name (alexandria:format-symbol (symbol-package name)
                                                                      "START-~a" name))
                                (stop-name (alexandria:format-symbol (symbol-package name)
                                                                     "STOP-~a" name))
                                (system-class 'test-system))
                          &body nodes)
    `(progn
       ,@(loop for (node-name . args) in nodes
               for system-name = (getf args :system-name (string-downcase node-name))
               for initargs = (getf args :initargs '())
               collect `(defparameter ,node-name (make-instance ',system-class :name ',system-name ,@initargs)))
       (defun ,start-name ()
         ,@(loop for (node-name . args) in nodes
                 for blocks = (getf args :blocks ''())
                 collect `(progn (start-system ,node-name)
                                 (dolist (block ,blocks)
                                   (apply #'put-block ,node-name block))))
         ,@(loop for (node-name . args) in nodes
                 for connections = (getf args :connected-to '())
                 collect `(progn ,@(loop for connection in connections
                                         collect `(attach-systems ,node-name ,connection)))))
       (defun ,stop-name ()
         ,@(loop for (node-name . args) in nodes
                 collect `(stop-system ,node-name)))))
