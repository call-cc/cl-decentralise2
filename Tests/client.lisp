(in-package :decentralise-tests)

(def-suite client)
(in-suite client)

(defvar *object1*
  '("test-object" 1 () "The quick brown fox jumps over the lazy dog"))
(defvar *object2*
  '("test-object" 2 () "The quicker browner fox jumps over the less quick less brown fox"))

(def-test client-tests ()
  (let ((system (make-instance 'test-system)))
    (start-system system)
    (let ((client (decentralise-client:connect-to-system system)))
      (unwind-protect
           (progn
             (apply #'put-block system *object1*)
             (is (equal (rest *object1*) (decentralise-client:get-block client (first *object1*))))
             (apply #'decentralise-client:put-block client *object2*)
             (is (equal (rest *object2*) (decentralise-client:get-block client (first *object2*))))
             (signals error (apply #'decentralise-client:put-block client *object1*)))
        (decentralise-client:stop-client client)
        (stop-system system)))))
