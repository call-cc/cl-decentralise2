(asdf:defsystem :decentralise2-tests
  :depends-on (:decentralise2 :alexandria :fiveam :uiop)
  :components ((:file "package")
               (:file "test-uri")
               (:file "synchronisation")
               (:file "kademlia")
               (:file "client")))
